package Maven_WebApp.ProductDetails;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class ShirtMaxTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		String[] value = new String[4];
		String[] colors = new String[2];
		String[] header = { "Style", "Color", "Size", "Price" };
		//70,54,57,18,41,61,47,52,39,43
		int[] totalColors = {70,54,57,18,41,61,47,52,39,43};
		String[] urls = {"https://www.shirtmax.com/adult-100-cotton-t-shirts/22-heavy-cotton-53-oz-t-shirt.html","https://www.shirtmax.com/adult-100-cotton-t-shirts/43-unisex-jersey-short-sleeve-t-shirt.html","https://www.shirtmax.com/adult-100-cotton-t-shirts/35-softstyle-45-oz-t-shirt.html","https://www.shirtmax.com/t-shirts/3058-unisex-heather-cvc-t-shirt.html","https://www.shirtmax.com/adult-5050-blend-t-shirts/25-dryblend-56-oz-5050-t-shirt.html",
				"https://www.shirtmax.com/adult-100-cotton-t-shirts/27-ultra-cotton-6-oz-t-shirt.html","https://www.shirtmax.com/adult-pullover-hooded-sweatshirts/553-heavy-blend-8-oz-5050-hood.html","https://www.shirtmax.com/kids-t-shirts/185-heavy-cotton-youth-53-oz-t-shirt.html","https://www.shirtmax.com/adult-100-cotton-t-shirts/29-52-oz-comfortsoft-cotton-t-shirt.html","https://www.shirtmax.com/adult-crewneck-sweatshirts/545-heavy-blend-8-oz-5050-fleece-crew.html"};
		System.setProperty("webdriver.chrome.driver","F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss");
		 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\competitors\\PriceListShirtMax"+sdf1.format(timestamp)+".csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);
			for(int u=0;u<10;u++)
			{
			driver.get(urls[u]);
			String model = driver.findElement(By.xpath("//dd/span[@itemprop='model']")).getText();
			System.out.println("Style-------------->" + model);
		
			for (int k = 1; k <= totalColors[u]; k++) {
				driver.findElement(By.xpath("(//div[@id='color-block']/ul/li)[" + k + "]")).click();
				String color = driver.findElement(By.xpath("//div/h5[@class='qo-color']")).getText();
				colors = color.split(":");
				System.out.println("Style---Color------->" + colors[1]);
				value[0] = model;
				value[1] = colors[1];
				
				for (int i = 1; i < 10; i++) {
					int j=2;
					boolean sizeExists = (driver.findElements(By.xpath("(//div[@class='price-col']/span[@class='item-size'])["+ i + "]")).size() != 0);
					if (sizeExists)
						value[j] = driver.findElement(By.xpath("(//div[@class='price-col']/span[@class='item-size'])["+ i + "]")).getText();
					else
						break;
					boolean priceExists = (driver.findElements(By.xpath("(//div[@class='price-col']/span[@class='color-row-price'])["+ i + "]")).size() != 0);
					if (priceExists)
						value[j+1] = driver.findElement(By.xpath("(//div[@class='price-col']/span[@class='color-row-price'])["+ i + "]")).getText();
					else
						value[j+1] = "*";
					System.out.println("value Size---->" + value[j]);
					System.out.println("value Price---->" + value[j+1]);
					writer.writeNext(value);
				}				
			}
			}
			//writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
