package Maven_WebApp.ProductDetails;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class ClothingShopTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		String[] value = new String[4];
		String[] colors = new String[2];
		String[] header = { "Style", "Color", "Size", "Price" };
		//70,65,62,65,41,63,46,52,31,46
		int[] totalColors = {70,65,62,65,41,63,46,52,31,46};
		String[] urls = {"https://www.clothingshoponline.com/p/gildan/5000_heavy-cotton-t-shirt","https://www.clothingshoponline.com/p/bella/3001_unisex-jersey-tee","https://www.clothingshoponline.com/p/gildan/64000_softstyle-t-shirt","https://www.clothingshoponline.com/p/bella/3001cvc_unisex-cvc-jersey-tee",
				"https://www.clothingshoponline.com/p/gildan/8000_dryblend-t-shirt","https://www.clothingshoponline.com/p/gildan/2000_ultra-cotton-t-shirt","https://www.clothingshoponline.com/p/gildan/18500_heavy-blend-hooded-sweatshirt",
				"https://www.clothingshoponline.com/p/gildan/5000b_heavy-cotton-youth-t-shirt",	"https://www.clothingshoponline.com/p/hanes/5280_comfortsoft-short-sleeve-t-shirt","https://www.clothingshoponline.com/p/gildan/18000_heavy-blend-crewneck-sweatshirt"};
		System.setProperty("webdriver.chrome.driver","F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\comp\\ProductClothing"+sdf1.format(timestamp)+".csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);
			for(int u=0;u<10;u++)
			{
			driver.get(urls[u]);
			String model = driver.findElement(By.xpath("//div[@id='M_M_zTitle']/h1")).getText();
			System.out.println("Style-------------->" + model);
		
			for (int k = 1; k <= totalColors[u]; k++) {
				driver.findElement(By.xpath("(//ul[@id='iSwatch']/li/a)[" + k + "]")).click();
				String color = driver.findElement(By.xpath("//div[@id='iColor']")).getText();
				colors = color.split(":");
				System.out.println("Style---Color------->" + colors[1]);
				value[0] = model;
				value[1] = colors[1];
				
				for (int i = 2; i < 11; i++) {
					int j=2;
					boolean sizeExists = (driver.findElements(By.xpath("(//div[@id='iGrid']/div/div[@class='iGname'])["+ i + "]")).size() != 0);
					if (sizeExists)
						value[j] = driver.findElement(By.xpath("(//div[@id='iGrid']/div/div[@class='iGname'])["+ i + "]")).getText();
					else
						break;
					boolean priceExists = (driver.findElements(By.xpath("(//div[@id='iGrid']/div/div[@class='iGprice'])["+ i + "]")).size() != 0);
					if (priceExists)
						value[j+1] = driver.findElement(By.xpath("(//div[@id='iGrid']/div/div[@class='iGprice'])["+ i + "]")).getText();
					else
						value[j+1] = "*";
					System.out.println("value Size---->" + value[j]);
					System.out.println("value Price---->" + value[j+1]);
					writer.writeNext(value);
				}				
			}
			}
			//writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
