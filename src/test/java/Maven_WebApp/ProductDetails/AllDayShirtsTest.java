package Maven_WebApp.ProductDetails;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class AllDayShirtsTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		String[] value = new String[4];
		String[] colors = new String[2];
		String[] header = { "Style", "Color", "Size", "Price" };
		//70,59,57,61,41,60,47,52,25,41
		//div[@class='swatchheading']/following-sibling::div[@class='swatchcolors']/ul/li  ==total colors
		//tr[not(contains(@style,'display: none'))]/td/a[@data-trigger='color']  ==total colors
		int[] totalColors = {70,59,57,61,41,60,47,52,25,41};
		String[] urls = {"https://www.alldayshirts.com/Adult-Heavy-Cotton-53-oz-T-Shirt_p_1450.html","https://www.alldayshirts.com/Unisex-Jersey-T-Shirt_p_883.html","https://www.alldayshirts.com/gildan-g640-adult-softstyle-t-shirt.html","https://www.alldayshirts.com/Unisex-Heather-CVC-T-Shirt_p_884.html",
				"https://www.alldayshirts.com/gildan-g800-adult-50-50-t-shirt.html","https://www.alldayshirts.com/gildan-g200-adult-ultra-cotton-t-shirt.html","https://www.alldayshirts.com/gildan-g185-blank-hoodie-sweatshirt.html","https://www.alldayshirts.com/Youth-Heavy-Cotton-53oz-T-Shirt_p_1451.html"
				,"https://www.alldayshirts.com/Unisex-52-oz-Comfortsoft-Cotton-T-Shirt_p_1506.html","https://www.alldayshirts.com/gildan-g180-fleece-crew-sweatshirt.html"};
		System.setProperty("webdriver.chrome.driver","F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\comp\\PriceListAllDayShirts"+sdf1.format(timestamp)+".csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);
			for(int u=0;u<10;u++)
			{
			driver.get(urls[u]);
			String model = driver.findElement(By.xpath("//div[@class='product-id']//span[@id='product_id']")).getText();
			System.out.println("Style-------------->" + model);
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			WebElement ele = driver.findElement(By.xpath("//div[@class='select_swatches']/label"));			
			try{
				driver.findElement(By.xpath("//div[@class='select_swatches']/label")).click();
			}catch(ElementClickInterceptedException e){
				//e.printStackTrace();
				WebDriverWait wait = new WebDriverWait(driver, 40);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='select_swatches']/label")));
				//driver.findElement(By.xpath("//div[@class='select_swatches']")).click();
				js.executeScript("arguments[0].click()", ele);
			}
			
			for (int k = 1; k <= totalColors[u]; k++) {
				//driver.findElement(By.xpath("(//a[@data-trigger='color'])[" + k + "]")).click();
				String color = driver.findElement(By.xpath("(//tr[not(contains(@style,'display: none'))]/td/a[@data-trigger='color'])[" + k + "]")).getText();
				System.out.println("Style---Color------->" + color);
				value[0] = model;
				value[1] = color;//*[@id="divOptionsBlock"]/div[1]/table/tbody/tr[1]/td[2]/div/text()
				
				for (int i = 2; i <= 9; i++) {
					int j=2;
				//	System.out.println("value Size---->" +driver.findElement(By.xpath("//*[@id='divOptionsBlock']/div[1]/table/tbody/tr["+ k +"]/td["+ i +"]/div")).getText());
					boolean sizeExists = (driver.findElements(By.xpath("(//*[@id='divOptionsBlock']/div[1]/table/tbody/tr[not(contains(@style,'display: none'))]/td["+i+"]/div[not(contains(text(),'sold out'))])["+k+"]")).size() != 0);
					if (sizeExists)
						value[j] = driver.findElement(By.xpath("(//*[@id='divOptionsBlock']/div[1]/table/tbody/tr[not(contains(@style,'display: none'))]/td["+i+"]/div[not(contains(text(),'sold out'))])["+k+"]")).getText();
					else
						break;
					boolean priceExists = (driver.findElements(By.xpath("(//*[@id='divOptionsBlock']/div[1]/table/tbody/tr[not(contains(@style,'display: none'))]/td["+i+"]/div[not(contains(text(),'sold out'))]/span)["+k+"]")).size() != 0);
					if (priceExists)
						value[j+1] = driver.findElement(By.xpath("(//*[@id='divOptionsBlock']/div[1]/table/tbody/tr[not(contains(@style,'display: none'))]/td["+i+"]/div[not(contains(text(),'sold out'))]/span)["+k+"]")).getText();
					else
						value[j+1] = "*";
					System.out.println("value Size---->" + value[j]);
					System.out.println("value Price---->" + value[j+1]);
					writer.writeNext(value);
				}				
			}
			}
			//writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
