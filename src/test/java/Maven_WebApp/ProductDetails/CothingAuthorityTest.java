package Maven_WebApp.ProductDetails;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class CothingAuthorityTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		String[] value = new String[4];
		String[] header = { "Style", "Color", "Size", "Price" };
		int[] totalColors = {33,54,70};
		String[] urls = {"https://clothingauthority.com/next-level-3600-unisex-cotton-t-shirt/","https://clothingauthority.com/bella-canvas-3001c-unisex-jersey-short-sleeve-t-shirt/","https://clothingauthority.com/gildan-g500-adult-heavy-cotton-5-3-oz-t-shirt/"};
		System.setProperty("webdriver.chrome.driver","F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\comp\\ProductClothAuthority.csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);
			for(int u=0;u<3;u++)
			{
			driver.get(urls[u]);
			String model = driver.findElement(By.xpath("//div[@class='TitleRight']/span")).getText();
			System.out.println("Style-------------->" + model);
		
			for (int k = 1; k <= totalColors[u]; k++) {
				try{
					driver.findElement(By.xpath("(//label[@class='form-option form-option-swatch' and not(contains(@style,'display: none'))]/span[@class='form-option-variant form-option-variant--color'])[" + k + "]")).click();
				}catch(ElementClickInterceptedException e){
					//e.printStackTrace();
					driver.findElement(By.xpath("//a[@class='last_line']")).click();
					WebDriverWait wait = new WebDriverWait(driver, 70);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//label[@class='form-option form-option-swatch' and not(contains(@style,'display: none'))]/span[@class='form-option-variant form-option-variant--color'])[" + k + "]")));
					driver.findElement(By.xpath("(//label[@class='form-option form-option-swatch' and not(contains(@style,'display: none'))]/span[@class='form-option-variant form-option-variant--color'])[" + k + "]")).click();
				}
				String color = driver.findElement(By.xpath("(//label[@class='form-option form-option-swatch' and not(contains(@style,'display: none'))]/span[@class='form-option-variant form-option-variant--color'])[" + k + "]")).getAttribute("title");
				System.out.println("Style---Color------->" + color);
				value[0] = model;
				value[1] = color;
				
				for (int i = 1; i < 10; i++) {
					int j=2;
					boolean sizeExists = (driver.findElements(By.xpath("(//label[@class='form-option']/span[@class='form-option-variant'])["+ i + "]")).size() != 0);
					if (sizeExists)
						value[j] = driver.findElement(By.xpath("(//label[@class='form-option']/span[@class='form-option-variant'])["+ i + "]")).getText();
					else
						break;
					boolean priceExists = (driver.findElements(By.xpath("(//label[@class='form-option']/span[@class='form-option-price'])["+ i + "]")).size() != 0);
					if (priceExists)
						value[j+1] = driver.findElement(By.xpath("(//label[@class='form-option']/span[@class='form-option-price'])["+ i + "]")).getText();
					else
						value[j+1] = "*";
					System.out.println("value Size---->" + value[j]);
					System.out.println("value Price---->" + value[j+1]);
					writer.writeNext(value);
				}				
			}
			}
			//writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
