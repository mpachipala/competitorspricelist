package Maven_WebApp.ProductDetails;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class JiffyProductsTest{

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException, IOException    {
		String[] value = new String[10];
		String[] header = { "Style", "Color","Size", "Price" };
		int[] totalColors = {35,69,64};
		String[] urls = {"https://www.jiffyshirts.com/nextlevel-3600.html?ac=Black","https://www.jiffyshirts.com/gildan-G500.html?ac=Black#?back=/?query=g500","https://www.jiffyshirts.com/bellacanvas-3001C.html?ac=Black"};
		System.setProperty("webdriver.chrome.driver", "F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\comp\\ProductJiffy.csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);
			for(int u=0;u<3;u++)
			{
			driver.get(urls[u]);
			String model = driver.findElement(By.xpath("(//div[@class='product-details-summary__items']/span[@class='product-details-summary__definition'])[1]")).getText();
			System.out.println("Style-------------->"+model);	
	
			for(int k=1;k<=totalColors[u];k++)
			{
				JavascriptExecutor js = ((JavascriptExecutor) driver);
				WebElement ele = driver.findElement(By.xpath("//span[contains(text(),'Pick Color')]"));
				js.executeScript("arguments[0].click()", ele);
				//driver.findElement(By.xpath("//span[contains(text(),'Pick Color')]")).click();		
				value[0]= model;
				value[1] =driver.findElement(By.xpath("(//a[@class='product-color-picker__item js-color-variant-selector']/div)["+k+"]")).getText();
				driver.findElement(By.xpath("(//a[@class='product-color-picker__item js-color-variant-selector'])["+k+"]")).click();
				System.out.println("Color-------------->"+value[1]);
				for(int i=1;i<9;i++)
				{
					int j=2;
					int p=i-4;
					int sp=p-1;
					boolean sizeExists = (driver.findElements(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']//label[@class='product-details-quantity-form-row__label product-details-quantity-form-row__label--new-layout'])["+ i + "]")).size() != 0);
					if (sizeExists){
						value[j] = driver.findElement(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']//label[@class='product-details-quantity-form-row__label product-details-quantity-form-row__label--new-layout'])["+ i + "]")).getText();
						if(value[j].equalsIgnoreCase("2XL") || value[j].equalsIgnoreCase("3XL")|| value[j].equalsIgnoreCase("4XL") || value[j].equalsIgnoreCase("5XL"))
						{
							boolean exists = (driver.findElements(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']//span/span[@class='-USD'])["+p+"]")).size() != 0);
							if(exists)
								value[j+1] = driver.findElement(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']//span/span[@class='-USD'])["+p+"]")).getText();
							else
								value[j+1] = driver.findElement(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']//span/span[@class='-USD'])["+sp+"]")).getText();
						}else
							value[j+1] = driver.findElement(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']//span/span[@class='-USD'])[1]")).getText();
					}else
						break;
			
					System.out.println("value Size---->"+value[j]);
					System.out.println("value Price---->"+value[j+1]);
					writer.writeNext(value);
				}			
			}
			}
			writer.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
