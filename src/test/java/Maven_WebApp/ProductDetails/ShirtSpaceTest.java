package Maven_WebApp.ProductDetails;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class ShirtSpaceTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException,Exception,
			IOException {
		String[] value = new String[4];
		String[] colors = new String[2];
		String[] header = { "Style", "Color", "Size", "Price" };
	//	70,65,63,65,41,62,47,52,26,47
		int[] totalColors = {70,65,63,65,41,62,47,52,26,47};
		String[] urls = {"https://www.shirtspace.com/gildan/t-shirts/1250-gildan-g500-adult-heavy-cotton-5-3-oz-t-shirt?variant_id=133677","https://www.shirtspace.com/bella-canvas/t-shirts/1998-bella-canvas-3001c-unisex-jersey-t-shirt?variant_id=118907","https://www.shirtspace.com/gildan/t-shirts/1257-gildan-g640-adult-softstyle-4-5-oz-t-shirt?variant_id=58559","https://www.shirtspace.com/bella-canvas/t-shirts/8496-bella-canvas-3001cvc-unisex-heather-cvc-t-shirt?variant_id=437462","https://www.shirtspace.com/gildan/t-shirts/1265-gildan-g800-adult-5-5-oz-50-50-t-shirt",
				"https://www.shirtspace.com/gildan/t-shirts/1230-gildan-g200-adult-ultra-cotton-6-oz-t-shirt","https://www.shirtspace.com/gildan/sweatshirts/1221-gildan-g185-adult-heavy-blend-8-oz-50-50-hood?variant_id=54783","https://www.shirtspace.com/gildan/t-shirts/1253-gildan-g500b-youth-heavy-cotton-5-3-oz-t-shirt","https://www.shirtspace.com/hanes/t-shirts/1836-hanes-5280-unisex-5-2-oz-comfortsoft-cotton-t-shirt","https://www.shirtspace.com/gildan/sweatshirts/1213-gildan-g180-adult-heavy-blend-adult-8-oz-50-50-fleece-crew"};
		System.setProperty("webdriver.chrome.driver","F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss");
		 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\competitors\\PriceListShirtSpace"+sdf1.format(timestamp)+".csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);
			for(int u=0;u<10;u++)
			{
			driver.get(urls[u]);
			String model = driver.findElement(By.xpath("//div[@class='p-title']/h1")).getText();
			System.out.println("Style-------------->" + model);
		
			for (int k = 1; k <= totalColors[u]; k++) {
				JavascriptExecutor js = ((JavascriptExecutor) driver);
				WebElement ele = driver.findElement(By.xpath("(//div[@class='all-colors-container']/div[@class='box']/span)[" + k + "]"));
				js.executeScript("arguments[0].click()", ele);
				//driver.findElement(By.xpath("(//div[@class='all-colors-container']/div[@class='box']/span)[" + k + "]")).click();
				String color = driver.findElement(By.xpath("//div[@class='attribute-box']/p[@class='chosen-color']")).getText();
				colors = color.split(":");
				System.out.println("Style---Color------->" + colors[1]);
				value[0] = model;
				value[1] = colors[1];
				
				for (int i = 1; i < 9; i++) {
					int j=2;
					boolean sizeExists = (driver.findElements(By.xpath("(//div/label[@class='variant-qty-box__label'])["+ i + "]")).size() != 0);
					if (sizeExists)
						value[j] = driver.findElement(By.xpath("(//div/label[@class='variant-qty-box__label'])["+ i + "]")).getText();
					else
						break;
					try{
						WebDriverWait wait = new WebDriverWait(driver, 4);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@role='row']/div/div[@class='variant-qty-box__price'])[" + i + "]")));
					boolean priceExists = (driver.findElements(By.xpath("(//div[@role='row']/div/div[@class='variant-qty-box__price'])[" + i + "]")).size() != 0);
					if (priceExists)
						value[j+1] = driver.findElement(By.xpath("(//div[@role='row']/div/div[@class='variant-qty-box__price'])[" + i + "]")).getText();
					else
						value[j+1] = "*";
					} catch(StaleElementReferenceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/*WebElement elePrice = driver.findElement(By.xpath("(//div[@role='row']/div/div[@class='variant-qty-box__price'])[" + i + "]"));
					value[j+1] =(String) js.executeScript("return arguments[0].innerText", elePrice);
					//value[j+1] = driver.findElement(By.xpath("(//div[@role='gridcell'])["+ i + "]")).getAttribute("aria-label");*/
					System.out.println("value Size---->" + value[j]);
					System.out.println("value Price---->" + value[j+1]);
					writer.writeNext(value);
				}				
			}
			}
			//writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
