package Maven_WebApp.ProductDetails;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class Jiffy{

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException, IOException    {
		String[] value = new String[10];
		String[] header = { "Style", "Color", "Price-S-XL", "Price-2XL", "Price-3XL", "Price-4XL","Price-5XL" };
		System.setProperty("webdriver.chrome.driver", "F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.jiffyshirts.com/nextlevel-3600.html?ac=Black");
		String model = driver.findElement(By.xpath("(//div[@class='product-details-summary__items']/span[@class='product-details-summary__definition'])[1]")).getText();
		System.out.println("Style-------------->"+model);	
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\comp\\ProductJiffy.csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);
	
	for(int k=1;k<=35;k++)
	{
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		 WebElement ele = driver.findElement(By.xpath("//span[contains(text(),'Pick Color')]"));
		js.executeScript("arguments[0].click()", ele);
		//driver.findElement(By.xpath("//span[contains(text(),'Pick Color')]")).click();		
		
		value[0]= model;
		value[1] =driver.findElement(By.xpath("(//a[@class='product-color-picker__item js-color-variant-selector']/div)["+k+"]")).getText();
		driver.findElement(By.xpath("(//a[@class='product-color-picker__item js-color-variant-selector'])["+k+"]")).click();
		
		for(int i=1;i<6;i++)
		{
			boolean exists = (driver.findElements(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']/div/div/div/span/span[@class='-USD'])["+i+"]")).size() != 0);
			if(exists)
			value[i+1] = driver.findElement(By.xpath("(//div[@class='product-details-quantity-form-row form-loading-placeholder']/div/div/div/span/span[@class='-USD'])["+i+"]")).getText();
			else
				value[i+1] = "*";
			System.out.println("value at"+i+"---->"+value[i+1]);
		}			
		writer.writeNext(value);
		}
			writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
