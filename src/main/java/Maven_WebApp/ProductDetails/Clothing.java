package Maven_WebApp.ProductDetails;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

public class Clothing {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		String[] value = new String[11];
		String[] header = { "Style", "Color", "Price-XS", "Price-S", "Price-M",
				"Price-L", "Price-XL", "Price-2XL", "Price-3XL", "Price-4XL","Price-5XL" };
		System.setProperty("webdriver.chrome.driver","F:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.clothingshoponline.com/p/bella/3001_unisex-jersey-tee");
		// driver.get("https://www.clothingshoponline.com/p/gildan/5000_heavy-cotton-t-shirt");
		String model = driver.findElement(By.xpath("//div[@id='M_M_zTitle']/h1")).getText();
		System.out.println("Style-------------->" + model);
		try (ICSVWriter  writer = new CSVWriterBuilder(new FileWriter("F:\\comp\\ProductClothing.csv")).withSeparator(',')
		          .build()) {
			writer.writeNext(header);

			for (int k = 1; k <= 65; k++) {
				driver.findElement(By.xpath("(//ul[@id='iSwatch']/li/a)[" + k + "]")).click();
				String color = driver.findElement(By.xpath("//div[@id='iColor']")).getText();
				System.out.println("Style---Color----------->" + color);
				value[0] = model;
				value[1] = color;

				for (int i = 2; i < 11; i++) {
					boolean exists = (driver.findElements(By.xpath("(//div[@id='iGrid']/div/div[@class='iGprice'])["+ i + "]")).size() != 0);
					if (exists)
						value[i] = driver.findElement(By.xpath("(//div[@id='iGrid']/div/div[@class='iGprice'])["+ i + "]")).getText();
					else
						value[i] = "*";
					System.out.println("value at" + i + "---->" + value[i]);
				}

				writer.writeNext(value);
			}
				writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

